# :guitar: Vault

## 1. **Install for Linux**

> URL : <https://learn.hashicorp.com/tutorials/vault/getting-started-install?in=vault/getting-started>

## 2. **Starting Vault Dev Mode**

    vault server -dev

## 3. **Key / Vaule Secret Engine**

- Enable secret for kv

      vault secrets enalbe -path=<secret-path> kv

- Disable secret

      vault secrets disable <secret-path>/

- Put key/value pair to secret

      vault kv put <secret-path>/<name> <key>=<vaule>

- List exsist secrets

      vault secrets list

- List kv stored secret

      vault kv list <secret-path>/

- Read kv stored secret

      vault read <secret-path>

- Get value of kv stored secret

      vault kv get <secret-path>/<name>

- Version up for kv

      vault kv enable-versioning <secrest-name>

- Get kv version

      vault kv get -version=<n> <secret-path>

- Delete kv

      vault kv delete <secret-path>

- Undelete kv

      vault kv undelete <secret-path>

- Get kv metadata

      vault kv metadata get <secret-path>

- Limit usable version

      vault kv metadata put -max-versions <n> <secret-path>

- TTLs

      vault kv put <secret-path> ttl=<s/m/h/d> <key>=<value>

## 4. **AWS Secret Engine**

**4-1**. **Setup**

- Enable Engine

      vault secrets enable -path=<path-name> aws

- Set Access-key $ Secret-key

      vault write <aws-engine-path>/config/root \
        access_key=<ACCESS_KEY> \
        secret_key=<SECRET_KEY> \
        region=ap-northeaset-1

- Set Vault Role assosiated IAM User

      vault write <aws-engine-path>/roles/<role-name> \
        credential_type=iam_user \
        policy_document=-<<EOF

        {
          "Version": "2012-10-17",
          "Statement": [
            {
              "Effect": "Allow",
              "Action": "*",
              "Resource": "*"
            }
          ]
        }

- Usage

      vault read <aws-engine-path>/creds/<role-name>

**4-4**. **Lease Revocation**

    vault lease revoke <my-lease-id>

    vault lease revoke -prefix <path>

## 5. **Transit Engine**

**5.1**. **Enable Transit Engine**

    vault secret enable transit

**5.2**. **Encrypt Data**

    vault write transit/encrypt/<path-name> plaintext=<base64-encrypted-data>

**5-3**. **Decrypt Data**

    vault write transit/decrypt/<path-name> ciphertext=<encrypted-data>

## 6. **Built-in Help**

    vault path-help <path>

## 7. **Authentication**

**7-1**. **Token authentication**

- Create a New Token

      vault token create

> Many Option: <https://www.vaultproject.io/docs/commands/token/create>

- Revoke the Token

      vault token revoke <token-id>

- Token Accessor

  - Lookup token capabilities
  - Lookup token properties
  - Revoke the token

- Token renew

      vault token renew

**7-2**. **GitHub authentication**

- Enable the GitHub auth method

      vault auth enable github

- Set the organization for the github authentication

      vault write auth/github/config organization=<org-name>

- Set Team Within Org and Mapping Policy to that Team

      vault write auth/github/map/teams/<team-name> value=<policy-name1>,<policy-name2>...

- Display all the authentication methods that Vault has enabled.

      vault auth list

- Help Command

      vault auth help <auth-type>

- Attempt to login with the github auth method

      vault login -method=github

- Revoke all tokens generated the github auth method

      vault token revoke -mode path auth/github

- Display the github auth method

      vault auth disable github

**7-3**. **userpass**

- Enable UserPass Authentication

      vault auth enable -path=<path> userpass

- Configure UserPass with Users

      vault write auth/<userpass-path>/users/<user-name> password=<password> policies=<policy-name>

## 8. **Policy**

**8-1**. **Basic**

- View the default policy

      vault policy read default

- Create the policy named my-policy with the contents from stdin.

      vault policy write my-policy - << EOF
      > path "secret/data/*" {
      > capabilities = ["create","update"]
      > }
      >
      > path "secret/data/foo" {
      > capabilities = ["read"]
      > }
      > EOF

- Create the policy from file which defined policy

      vault policy write <policy-name> <policy-file-name>

- List all the policies

      vault policy list

- View the contents of policy

      vault policy read <policy-name>

- Create a token and add the policy created

      vault token create -policy=<policy-name>

- How to associate policy already defined to some user example

      vault write auth/userpass/<users>/<user1> \
      password=<password1> \
      policies=<policy1>

**8-2**. **Associate Policies to Auth Methods**

- check to verify that approle auth method has not been enabled at the path approle/

      vault auth list | grep 'approle/'

- enable it before proceeding

      vault auth enable approle

**8.3**. **Summarizing the Path**

| Description                              | Path               |
| ---------------------------------------- | ------------------ |
| Writing adn Reading Version              | data/              |
| Listing keys                             | metadata/ [list]   |
| Reading Versions                         | metadata [read]    |
| Destroy Versions of Secret               | destroy/ [update]  |
| Destroy all versions of metadata for key | metadata/ [delete] |

**8.4**. :dizzy: **AppRole** :dizzy:

- 1 . Enable AppRole Secret Engine

      vault auth enable -path=<approle-name> approle

- 2 . Create Policy

      vault policy write <policy-name> -<< EOF
      > path "secret/*" {
      > capabilities = ["create","read","update","delete"]
      > }
      > EOF

- 3 . Create Role and Associate with Policy

      vault write auth/approle/role/<role-name> \
          secret_id_ttl=<time> \
          token_num_uses=<num> \
          token_ttl=<time> \
          token_max_ttl=<time> \
          secret_id_num_uses=<num>
          token_policies="<policy-name>"

- 4 . Get Role-id

      vault read auth/approle/role/<role-name>/role-id

- 5 . Generate Secret ID

      vault write -f auth/approle/role/jenkins/secret-id

- 6 . Authenticat with Role ID and Secret ID

      vault write auth/approle/login role_id="<ROLE_ID>" secret_id="<SECRET_ID>"

## 9. Vault Deployment Guide with Integrated Storage

**9-1**. **Download Vault Binaries**

> URL : <https://releases.hashicorp.com/vault/>

**9-2**. **Install Vault**

      unzip vault_${VAULT_VERSION}_linux_amd64.zip
      sudo chown root:root vault
      sudo mv vault /usr/local/bin/
      Vault v1.4.0+ent
      vault -autocomplete-install
      complete -C /usr/local/bin/vault vault
      sudo setcap cap_ipc_lock=+ep /usr/local/bin/vault
      sudo useradd --system --home /etc/vault.d --shell /bin/false vault

> :beetle: Refefence :beetle: ---> _vault.txt_

**9-3**. **Configure Vault Service**

      sudo mkdir --parents /etc/vault.d
      sudo touch /etc/vault.d/vault.hcl
      sudo chown --recursive vault:vault /etc/vault.d
      sudo chmod 640 /etc/vault.d/vault.hcl
      sudo vi /etc/vault.d/vault.hcl
      sudo mkdir /opt/raft
      sudo chown -R vault:vault /opt/raft

**9-4**. **Configure Vault Process**

      sudo touch /etc/systemd/system/vault.service

> :beetle: Refefence : :beetle: ---> _systemd-vault.txt_

**9-5**. **Start Vault Service**

      sudo systemctl enable vault --now
      sudo systemctl status vault
      export VAULT_ADDR="http://<ip-addr>:8200"

## 10. Install vault for single cluster for ubuntu

> URL : <https://learn.hashicorp.com/tutorials/vault/getting-started-install?in=vault/getting-started>

- **Initialization**

      vault operator init

      vault operator init -key-shares=<num> -key-threshold=<num>

      vault operator unseal

      vault login <initial-root-token>

> 「**vault operator rekey**」is able to issue new key

## 11. HTTP API

| Capabilitiy | Associated HTTP Verb |
| ----------- | -------------------- |
| create      | POST / PUT           |
| list        | GET                  |
| update      | POST / PUT           |
| delete      | DELETE               |
| list        | LIST                 |

> URL : <https://www.vaultproject.io/api>

## 12. Toekn Capability

- Check Current Token

      vault print token

- Check Token Capability at the Path

      vault token capability <path>

## 13. Entity and Group

- Structure

      Groups ---> Entities ---> users

## 14. Path Template

- Example

      path "secret/data/{{identity.entity.name}}/*" {
        capabilities = ["create","read","update","delete"]
      }

> ※ Template needs to be attached to each entity object.

## 15. Vault Policy Rules - Transit Engine

- Write

      vault write -f transit/key/<key-name>

- Encyrpt Data

      vault write transit/encrypt/<key-name> plaintext=$(base64 <<< <secret-data>)

- Decrypt Data

      vault write transit/decrypt/<key-name> ciphertext=<encrypted-data>

- Require Priviledges for User Associated with Policy to use

      path "transit/encrypt/<key-name>" {
        capabilities = ["update"]
      }

      path "transit/decrypt/<key-name>" {
        capabilities = ["update"]
      }

      path "transit/keys" {
        capabilities = ["list"]
      }

      path "transit/keys/<key-name>" {
        capabilities = ["read"]
      }

## 16. Vault Tokens

- Lookup Token Information

      vault token lookup

- Create Token with Explicit TTL

      vault token create --ttl=<time>

> :honeybee: 「-help」 flag show many option available  
> e.g.) 「-ttl=\<duration>」、「-type=\<string>」

- Renewing a Token

      vault token renew --increment=<time> <token-id>

- Default TTL Information

      vault read sys/auth/token/tune

      vault read sys/mounts/auth/token/tune

- Accessor

      vault list auth/token/accessor

      vault token lookup -accessor <accessor-id>

- Orphaned Token

      vault token create -orphan

  > :palm_tree: Orpahned Token require some priviledge like below to issue

      path "auth/token/create" {
        capabilities = ["create","read","update","sudo"]
      }

      path "auth/token/lookup" {
        capabilities = ["create","read","update"]
      }

## 17. Response Wrapping

### :satellite: Use Case

・・・More Securly doing AppRole Operation

- Administrator

  1. Mount approle auth backend
  2. Create policy and role for app

- Trusted Entity(Terraform,k8s etc)

  1. Get Role ID
  2. Deliver Role ID to APP
  3. **Get wrapped Secret ID**
  4. **Return wrapping token**
  5. Deliver wrapping token to app

- APP

  1. **Unrwrap Secret ID**
  2. Login with Role ID & Secret ID
  3. Return Token from vault

- Create Wrapping Token

      vault token create -wrap-ttl=<time>

- Unrwap Wrapping Token

      vault unwrap <unwrapped-token-id>

## 18. Batch Token

- Batch Token

      vault token create -type=batch -policy=<policy-name>

|                                                        | Service Token                                              | Batch Token                                        |
| ------------------------------------------------------ | ---------------------------------------------------------- | -------------------------------------------------- |
| Can Be Root Token                                      | :o:                                                        | :heavy_multiplication_x:                           |
| Can Create Child Token                                 | :o:                                                        | :heavy_multiplication_x:                           |
| Can Be Renewable                                       | :o:                                                        | :heavy_multiplication_x:                           |
| Can Be Periodic                                        | :o:                                                        | :heavy_multiplication_x:                           |
| Can Have Explicit Max TTL                              | :o:                                                        | :heavy_multiplication_x:                           |
| Has Accessors                                          | :o:                                                        | :heavy_multiplication_x:                           |
| Has Cubbyhole                                          | :o:                                                        | :heavy_multiplication_x:                           |
| Revoked With Parent(if not orphan)                     | :o:                                                        | Stop Working                                       |
| Dynamic Secret Lease Assignment                        | Self                                                       | Parent(if not orphan)                              |
| Can Be Used Across Performance<br>Replication Cluster  | :heavy_multiplication_x:                                   | :o:(if orphan)                                     |
| Creation Scales With Performance<br>Standby Node Count | :heavy_multiplication_x:                                   | :o:                                                |
| Cost                                                   | Heavtweigh;<br>multiple storage writes per token creattion | Lightweight;<br>no storage cost for token creation |

## 19. TTL Configuration

- Token TTL Configuration(CLI)

      vault write sys/mounts/auth/token/tune default_lease_ttl=<default_lease_time> max_lease_ttl=<max_lease_time>

- Token TTL parameter change(CLI)

      vault write sys/mounts/auth/token/tune default_lease_ttl=<time> max_lease_ttl=<time>

## 20. Periodic Token

- Periodic Token(CLI)

      vault token create -period=<time> -policy=<policy-name>

:ghost: **Periodic Token never expire proviced that they are renewed**

## 21. :honeybee: Vault Agent

> URL: <https://www.vaultproject.io/docs/agent>

    cat vault-agent.hcl

    pid_file = "./pidfile"

    vault {
      address = "http://<vault-sv-addr>:8200"
    }

    auto_auth {
      method "approle" {
        mount_path = "auth/approle"
        config = {
         role_id_file_path = "/tmp/role-id"
          secret_id_file_path = "/tmp/secret-id"
          remove_secret_id_file_after_reading = false
        }
      }

      sink "file" {
        config = {
          path = "/tmp/token"
        }
      }
    }

> 「/tmp/role-id」 include role-id & 「/tmp/secret-id」 include secret-id for app-role

## 22. Audit Devices

- Type of Audit Devices

  - File
  - Syslog
  - Socket

- Enable Audit File

      vault audit enable file file_path=<file-path>

- Enable logs on stdout

      vault kv put <K/V path> audit_device=file

## 23. Root Protected API Endpoints

- auth/token/accessors
- auth/token
- sys/audit
- sys/rotate
- sys/seal

## 24. Reading Secret Metadata

- KV Version 2

      path "<secret-path>/medata/<secret-name> {
        capabilities = ["read"]
      }

## 25. Unsealing with Transit Auto Unseal

> URL : <https://www.vaultproject.io/docs/configuration/seal/transit>
