# :star2: Vault Exam

## :dolphin: Hashicorp Certified Vault Associate 2021

- 質問２：Hashicorp のテクニカルサポートが受けられるストレージバックエンド

|                | HashiCorp Supported | High Availability |
| -------------- | ------------------- | ----------------- |
| **Consul**     | :o:                 | :o:               |
| **Filesystem** | :o:                 | :x:               |
| **In-Memory**  | :o:                 | :x:               |
| **Raft**       | :o:                 | :o:               |
| **Etcd**       | :x:                 | :o:               |
| **Dynamo DB**  | :x:                 | :o:               |

- 質問４：バッチトークン使用が推奨されるアクション  
  ・・・「**Encryption Data**」

- 質問６：Secret Engineで実行できるアクションタイプ

  - Store・・・K/V
  - Compress・・・
  - Encrypt・・・Transit
  - Generate・・・AWS Engine

- 質問８：Vault Enterprise Replication

・**_Performance Replication and Disaster Recovery (DR) Replication_**

| Capability                                       | **Disaster Recovery** | **Performance** |
| ------------------------------------------------ | --------------------- | --------------- |
| プライマリへのコンフィグ同期                     | :o:                   | :o:             |
| プライマリバックエンドに格納されたコンフィグ同期 | :o:                   | :o:             |
| プライマリへのトークンやリース時間などの同期     | :o:                   | :x:             |
| セカンダリのクライアントリクエストの処理         | :x:                   | :o:             |

・**_Primary and Secondary Cluster Compatibility_**

| Primary seal | Secondary seal | Impact on secondary of enabling replication                                                    |
| ------------ | -------------- | ---------------------------------------------------------------------------------------------- |
| Shamir       | Shamir         | **Seal config and unseal keys replaced with primary's**                                        |
| Shamir       | Auto           | **Seal recovery config and recovery keys replaced with primary's seal config and unseal keys** |
| Auto         | Auto           | **Seal recovery config and recovery keys replaced with primary's**                             |
| Auto         | Shamir         | **Seal config and unseal keys replaced with primary's recovery seal config and recovery keys** |

- 質問２３：vault でプラグインを使用する場合  
  ・・・vault.hcl に「**plugin_file = \<path>**」と指定する。

- 質問２４：vault クラスターのリーダーを確認する場合の API
  パス  
  ・・・「**/sys/leader**」

- 質問２８：K/V v2 における CLI コマンドの機能

      vault kv destroy　→　「特定のバージョン」のsecretを完全削除

      vault kv delete　→　secretのソフトdelete

      vault kv metadata delete　→　secretの完全削除

- 質問３８：自動 SEAL 解除機能を持つ KMS

  1. Transit
  2. Azure KMS
  3. AWS KMS
  4. GCP KMS
  5. AliCloud KMS
  6. OCI KMS
  7. HCM

- 質問５１：root トークンを使用するのが適切なオペレーション

  1. root トークンとは別のアドミニストレーション用トークンの発行
  2. **Audit Devices の有効化**

- 質問６６：/sys/seal エンドポイントでオペレーションを実施する際に必要な capability  
  **・・・「sudo」**

## :tropical_fish: HashiCorp Certified: Vault Associate - Exam #1

- 質問６：Parameter Constraints

  1.  **required_parameters**  
      ・・・A list of parameters that must be specified.

             # This requires the user to create "secret/foo" with a parameter named
             # "bar" and "baz".
             path "secret/foo" {
               capabilities = ["create"]
               required_parameters = ["bar", "baz"]
             }

  2.  **allowed_parameters**  
      ・・・Whitelists a list of keys and values that are permitted on the given path.

             # This allows the user to create "secret/foo" with a parameter named
             # "bar". It cannot contain any other parameters, but "bar" can contain
             # any value.
             path "secret/foo" {
               capabilities = ["create"]
               allowed_parameters = {
                 "bar" = []
               }
             }

> URL: <https://www.vaultproject.io/docs/concepts/policies#parameter-constraints>

- 質問６：Rotate Transit Key's Version

      vault write -f transit/keys/<key-name>/rotate

- 質問７：The Revocation of Lease

  - API
  - CLI
  - TTL Expire

> NOTE: Lease can't be revoke in the UI

- 質問２０：Initialize Vault Problem  
  ・・・vault を初期化する際、初期化を実施する構築者が unseal するキーをすべて見ることができ、かつその unseal 用のキーは暗号化されていないため、鍵の配布に問題が発生する。  
  以下のオプションを付与することで、初期化を担当する構築者がすべての unseal キーを知ることなく、かつ、unseal キーを暗号化した状態で unseal を担当する担当者へ安全に配布することが可能になる。

  「**pgp-keys**」：PGP キー所有者と PGP キーのパスを指定

        vault operator init -key-shares=<num> -key-threshold=<num> -pgp-keys="keybase:<name1>,keybase=<name2>,keybase:<name3>・・・"

- 質問２６：Required Capabilities for Dynamic Credentials on Database
  ・・・GET = 「**read**」capability is needes.

- 質問３４：Required Attribute for Renewing Token

  - token_id = token
  - token accessor

- 質問５０：Re-Encrypt the data for Using Transit Key's Version

      vaul write transit/rewap/<key-name> ciphertext=<old-data>

## :whale: HashiCorp Certified: Vault Associate - Exam #2

- 質問３：Transit Engineの使用

      vault secrets enable transit

      vault write -f transit/keys/<key-name>

      vault write transit/encryption/<key-name> plaintext=$(base64 <<< <plaintext-name>)

- 質問６：Periodic Token  
  ・・・Periodic **Token は TTL を持っているが**、**max TTL は持っていない**。そのため、**TTL を更新すれば半永久的に使用可能**。  
  ※**root または sudo 権限を持ったユーザーのみが発行できる**。

- 質問２３：シークレットを強制的に除去するコマンド

      vault lease revoke -force -prefix <lease-path>

- 質問２５：sys/  
  ・・・Vault のすべてのバックエンドシステム機能は「sys/」にマウントされている

- 質問３４：初期化、再起動後の Vault クラスター  
  ・・・暗号化鍵がいったんメモリ内に格納された後は、Vault ノードはそれらの鍵をクラスター内の別ノードで共有したり、複製したりはしないため、**初期化後、または再起動後は個別に unseal 作業を実施する必要がある。**

- 質問３６：operator step-down  
  ・・・「**operator step-down**」はクラスター内のアクティブノードを強制的にダウンさせる。

- 質問４３：Transit Engine

  1. 有効化

     vault secrets enable transit

  2. 暗号化鍵の作成

     vault write -f transit/keys/<key-name>

  3. Base64 でエンコード

     base64 <<< "<something>・・・"

  > URL : <https://www.vaultproject.io/docs/secrets/transit>

- 質問４６：CLI での policy の作成

      vault policy write <poilcy-name> <policy-file>.hcl

      vault policy write <policy-name> -<< EOF
        path "<path-name>" {
          capabilities = [<capabilities-list>]
        }

      cat <policy-file>.hcl | vault policy write <policy-name> -

## HashiCorp Certified: Vault Associate - Exam #3

- 質問６： Encrypt / Decrypt できる Secret Engine
  ・・・「**transit**」と「**transform**(only Enterprise Edition)」のみ

- 質問９：vault agent が提供する機能

  - Auto-Auth
  - Cacheing
  - **Templating**

- 質問：１１ namespace の API Endpoint  
  ・・・例）namespace「integration」の場合

  - X-Vault-Namespace Header オプション指定

        curl \
        --header "X-Vault-Token:<token> \
        --header "X-Vault-Namespace: integration" \
        --request GET \
        https://<vault.example.com:8200>/v1/secret/data/<secret-name>

  - API Endpoint 指定

        curl \
        --header "X-Vault-Token: <token> \
        --request GET \
        https://<vault.example.com:8200>/v1/integration/secret/data/<secret-name>

- 質問１３：Token タイプ

  1. **Service Token**  
     ・・・一般的なトークン
  2. **Batch Token**  
     ・・・サイズが大きなバイナリデータ等を暗号化する場合に使用される
  3. **Periodic Token**  
     ・・・TTL はあるが、max _TTL を持たないトークン_
  4. **Orphan Token**  
     ・・・親トークンが expire しても expire しないトークン
  5. **Root Token**

- 質問１８：API Endpoint for namespace

  1. Include API Endpoint in HTTP request URL

         curl \
         --header "X-Vault-Token: <token-id> \
         --request GET \
         https://<vault-sv-url>:8200/v1/<namespace-name>/<secret-path>

  2. Add X-Vault-Namespace Header

         curl \
         --header "X-Vault-Token: <token-id> \
         --header "X-Vault-Namespace": "<namespace>" \
         --request GET
         https://<vault-sv-url>:8200/v1/<namespace-name>/<secret-path>

- 質問２１：vault secrets tune

> :frog:URL: <https://www.vaultproject.io/docs/commands/secrets/tune>

- 質問２３：vault-agent feature

  1. Auto-Auth
  2. Caching
  3. **Templating**

- 質問２５：Token Accessor

    1. Lookup a token's properties(**not including the actual token ID**)
    2. Lookup a token's capabilities on a path
    3. Renew a token
    4. Revoke a token

- 質問３９：The thing lease ID can do

  - revoke
  - renew

- 質問５７：The type of auth method needed by

  - **Machine-oriented**  
  ・・・Approle、TLS、Token、platform-specific-method(cloud,k8s etc)

  - **Operator-oriented**  
  ・・・Github、LDAP、username & password